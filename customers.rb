class Customers

attr_accessor :name, :wallet, :choice, :age, :drunkness


def initialize(name, wallet, choice, age, drunkness)
@name = name
@wallet = wallet
@choice = choice
@age = age
@drunkness = drunkness
end

def buy_drink(drink)
  @wallet -= drink.price
end

def gets_drunk(drink)
  @drunkness += drink.alc_level
end




end
