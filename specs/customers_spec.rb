require('minitest/autorun')
require('minitest/rg')
require_relative('../customers')
require_relative('../drinks')

class CustomersTest < Minitest::Test

def setup
@drink1 = Drinks.new("beer", 4, 7)
@drink2 = Drinks.new("wine", 3, 10)
@drink3 = Drinks.new("cider", 2, 5)

@choice = [@drink1, @drink2, @drink3]
@customer1 = Customers.new("Bob", 10, "beer", 18, 0)
@customer2 = Customers.new("John", 15, "cider", 30, 0)
@customer3 = Customers.new("Mary", 20, "wine", 17, 0)
end

def test_customer_has_a_name
assert_equal("Bob", @customer1.name)
end

def test_customer_has_money
assert_equal(15, @customer2.wallet)
end

def test_customer_has_a_drink
assert_equal("wine", @customer3.choice)
end

def test_check_age
assert_equal(18, @customer1.age)
end

def test_buy_drink
@customer1.buy_drink(@drink1)
assert_equal(6, @customer1.wallet)
end

def test_check_steaming
  @customer1.gets_drunk(@drink1)
  @customer1.gets_drunk(@drink2)
  assert_equal(17, @customer1.drunkness)
end

end
