require('minitest/autorun')
require('minitest/rg')
require_relative('../drinks')

class DrinksTest < Minitest::Test

def setup
  @drink1 = Drinks.new("beer", 4, 7)
  @drink2 = Drinks.new("wine", 3, 10)
  @drink3 = Drinks.new("cider", 2, 5)
end

def test_drink_name
assert_equal("wine", @drink2.type)
end

def test_drink_price
assert_equal(4, @drink1.price)
end



end
