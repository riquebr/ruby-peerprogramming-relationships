require('minitest/autorun')
require('minitest/rg')
require_relative('../pub')
require_relative('../drinks')
require_relative('../customers')

class PubTest < Minitest::Test

def setup

@drink1 = Drinks.new("beer", 4)
@drink2 = Drinks.new("wine", 3)
@drink3 = Drinks.new("cider", 2)

@choice = [@drink1, @drink2, @drink3]

@customer1 = Customers.new("Bob", 10, "beer", 18)
@customer2 = Customers.new("John", 15, "cider", 30)
@customer3 = Customers.new("Mary", 20, "wine", 17)

@pub = Pub.new("Clansman", 100, @choice)
end

def test_pub_has_a_name
assert_equal("Clansman", @pub.pub_name)
end

def test_pub_has_money
assert_equal(100, @pub.till)
end

def test_pub_has_drinks                 ## Ask about this, why my array is not going if it's local.
assert_equal(@choice, @pub.selection)
end

def test_get_drink_order
@pub.get_drink(@customer2, @drink1)
assert_equal(104, @pub.till)
assert_equal(11, @customer2.wallet)
end

def test_check_age_of_customer__true
assert_equal(true, @pub.check_age_of_customer(@customer1))
end

def test_check_age_of_customer__false
assert_equal(false, @pub.check_age_of_customer(@customer3))
end


end
